from entities.recipe import Recipe
from entities.snack import Snack

DAYS = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday',
    'Sunday']

class MealPlanPresenter(object):
    def __init__(self, db):
        self.db = db

    def getViewModel(self, weekRecipes):
        viewModel = {}
        people = self.db.getPeople()
        for person in people:
            weekMeals = {}

            for j, dayName in enumerate(DAYS):
                dayDict = {}
                dayMealsList = []
                dayMeals = weekRecipes[j]
                for i, blocks in enumerate(person['blocks']):
                    mealDict = {}
                    meal = dayMeals[i]
                    try:
                        recipe = Recipe.fromDict(meal)
                        ingredients = recipe.getIngredients(blocks)
                    except AttributeError:
                        recipe = Snack.fromDict(meal)
                        ingredients = recipe.getIngredients()

                    mealDict['name'] = recipe.getName()
                    mealDict['ingredients'] = [i.toDict() for i in ingredients]
                    dayMealsList.append(mealDict)

                weekMeals[dayName] = dayMealsList

            viewModel[person['name']] = weekMeals

        return viewModel

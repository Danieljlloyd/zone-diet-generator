from actions.grocery_list_actions import GroceryListActions

class GroceryListPresenter(object):
    def __init__(self, db):
        self.db = db
        self.groceryActions = GroceryListActions()

    def getViewModel(self, weekRecipes):
        viewModel = {}
        people = self.db.getPeople()
        for person in people:
            groceries = self.groceryActions.addWeeklyGroceries(person['blocks'],
                weekRecipes)
            viewModel[person['name']] = groceries

        totalGroceries = {}
        for name, groceries in viewModel.iteritems():
            for foodItem, data in groceries.iteritems():
                if foodItem not in totalGroceries.keys():
                    totalGroceries[foodItem] = data
                else:
                    totalGroceries[foodItem]['quantity'] += data['quantity']

        viewModel['total'] = totalGroceries

        return viewModel

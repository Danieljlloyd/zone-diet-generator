import unittest
from database.memory_database import MemoryDatabase
from controllers.recipe_generation import RecipeGenerationController

class TestRecipeGenerationController(unittest.TestCase):
    def testGenerateWeekOfRecipes(self):
        db = MemoryDatabase()
        controller = RecipeGenerationController(db)
        weekRecipes = controller.getWeekRecipes()

import unittest
from entities.ingredient import Ingredient

class TestIngredient(unittest.TestCase):
    def setUp(self):
        self.ingredient = Ingredient(
            name='corn tortilla',
            quantity=1,
            metric='pieces',
        )

        self.ingredientTwo = Ingredient(
            name='hard boiled egg',
            quantity=2,
            metric='pieces',
        )

    def testGetName(self):
        self.assertEqual(self.ingredient.getName(), 'corn tortilla')
        self.assertEqual(self.ingredientTwo.getName(), 'hard boiled egg')

    def testGetQuantity(self):
        self.assertEqual(self.ingredient.getQuantity(), 1)
        self.assertEqual(self.ingredientTwo.getQuantity(), 2)

    def testGetMetric(self):
        self.assertEqual(self.ingredient.getMetric(), 'pieces')

    def testCopyEquality(self):
        newIngredient = self.ingredient.copy()
        self.assertEqual(newIngredient, self.ingredient)

    def testToDict(self):
        self.assertEqual(self.ingredient.toDict(),
            {
                'name': 'corn tortilla',
                'quantity': 1,
                'metric': 'pieces',
            })

    def testFromDict(self):
        self.assertEqual(self.ingredient, Ingredient.fromDict({
                'name': 'corn tortilla',
                'quantity': 1,
                'metric': 'pieces',
            }))

import unittest

from database.memory_database import MemoryDatabase
from controllers.recipe_generation import RecipeGenerationController
from presenters.meal_plan import MealPlanPresenter

class TestMealPlanPresenter(unittest.TestCase):
    def setUp(self):
        self.db = MemoryDatabase()
        self.controller = RecipeGenerationController(self.db)
        self.presenter = MealPlanPresenter(self.db)

    def testViewModel(self):
        weekRecipes = self.controller.getWeekRecipes()
        viewModel = self.presenter.getViewModel(weekRecipes)
        #import json
        #print(json.dumps(viewModel, indent=2))

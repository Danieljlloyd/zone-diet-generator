import unittest
from entities.grocery_list import GroceryList
from entities.ingredient import Ingredient

class TestGroceryList(unittest.TestCase):
    def setUp(self):
        self.groceryList = GroceryList()
        self.ingredientOne = Ingredient(
            name='corn tortilla',
            quantity=1,
            metric='pieces',
        )
        self.ingredientTwo = Ingredient(
            name='hard boiled egg',
            quantity=2,
            metric='pieces',
        )

    def testInitialise(self):
        self.assertEqual(self.groceryList.getGroceries(), {})

    def testAddIngredient(self):
        self.groceryList.addIngredient(self.ingredientOne)
        self.assertEqual(len(self.groceryList.getGroceries().keys()), 1)

    def testAddSameIngredients(self):
        self.groceryList.addIngredient(self.ingredientOne)
        self.groceryList.addIngredient(self.ingredientOne)
        self.assertEqual(len(self.groceryList.getGroceries().keys()), 1)

        groceries = self.groceryList.getGroceries()
        self.assertEqual(groceries['corn tortilla']['quantity'], 2)

    def testTwoIngredients(self):
        self.groceryList.addIngredient(self.ingredientOne)
        self.groceryList.addIngredient(self.ingredientTwo)
        self.assertEqual(len(self.groceryList.getGroceries().keys()), 2)

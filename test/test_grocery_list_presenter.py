import unittest
from database.memory_database import MemoryDatabase
from presenters.grocery_list import GroceryListPresenter
from controllers.recipe_generation import RecipeGenerationController

class TestGroceryListPresenter(unittest.TestCase):
    def setUp(self):
        self.db = MemoryDatabase()
        self.controller = RecipeGenerationController(self.db)
        self.presenter = GroceryListPresenter(self.db)

    def testViewModel(self):
        weekRecipes = self.controller.getWeekRecipes()
        viewModel = self.presenter.getViewModel(weekRecipes)

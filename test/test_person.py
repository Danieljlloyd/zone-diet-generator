import unittest
from entities.person import Person

class TestPerson(unittest.TestCase):
    def testGetName(self):
        person = Person(
            name='Shekar Seldon',
            blocks=[3, 3, 1, 3, 1],
        )
        self.assertEqual(person.getName(), 'Shekar Seldon')

    def testGetBlocks(self):
        person = Person(
            name='Shekar Seldon',
            blocks=[3, 3, 1, 3, 1],
        )
        self.assertEqual(person.getBlocks(), [3, 3, 1, 3, 1])

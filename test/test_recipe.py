import unittest
from entities.ingredient import Ingredient
from entities.recipe import Recipe

class TestRecipe(unittest.TestCase):
    def setUp(self):
        self.ingredients = {
            1: [
                Ingredient(
                    name='corn tortillas',
                    quantity=2.0,
                    metric='pieces',
                ),
                Ingredient(
                    name='black beans',
                    quantity=0.5,
                    metric='cups',
                ),
                Ingredient(
                    name='onions',
                    quantity=0.33,
                    metric='pieces',
                ),
                Ingredient(
                    name='green pepper',
                    quantity=1,
                    metric='pieces',
                ),
                Ingredient(
                    name='eggs',
                    quantity=3,
                    metric='pieces',
                ),
                Ingredient(
                    name='cheese',
                    quantity=2,
                    metric='ounces',
                ),
                Ingredient(
                    name='avocado',
                    quantity=5,
                    metric='tbsp',
                ),
            ]
        }

        self.recipe = Recipe(
            name='Breakfast Quesadilla',
            ingredients=self.ingredients.copy()
        )

    def testGetIngredients(self):
        self.assertEqual(self.recipe.getIngredients(1), self.ingredients[1])

    def testGetName(self):
        self.assertEqual(self.recipe.getName(), 'Breakfast Quesadilla')

    def testCopyEquality(self):
        self.assertEqual(self.recipe.copy(), self.recipe)

    def testRecipeToDict(self):
        self.maxDiff=None
        self.assertEqual(self.recipe.toDict(),
            {
                'name': 'Breakfast Quesadilla',
                'ingredients': {
                    '1': [
                        {
                            'name': 'corn tortillas',
                            'quantity': 2,
                            'metric': 'pieces',
                        },
                        {
                            'name': 'black beans',
                            'quantity': 0.5,
                            'metric': 'cups',
                        },
                        {
                            'name': 'onions',
                            'quantity': 0.33,
                            'metric': 'pieces',
                        },
                        {
                            'name': 'green pepper',
                            'quantity': 1,
                            'metric': 'pieces',
                        },
                        {
                            'name': 'eggs',
                            'quantity': 3,
                            'metric': 'pieces',
                        },
                        {
                            'name': 'cheese',
                            'quantity': 2,
                            'metric': 'ounces',
                        },
                        {
                            'name': 'avocado',
                            'quantity': 5,
                            'metric': 'tbsp',
                        },
                    ]
                }
            })

    def testFromDict(self):
        self.assertEqual(self.recipe, Recipe.fromDict(
             {
                'name': 'Breakfast Quesadilla',
                'ingredients': {
                    '1': [
                        {
                            'name': 'corn tortillas',
                            'quantity': 2,
                            'metric': 'pieces',
                        },
                        {
                            'name': 'black beans',
                            'quantity': 0.5,
                            'metric': 'cups',
                        },
                        {
                            'name': 'onions',
                            'quantity': 0.33,
                            'metric': 'pieces',
                        },
                        {
                            'name': 'green pepper',
                            'quantity': 1,
                            'metric': 'pieces',
                        },
                        {
                            'name': 'eggs',
                            'quantity': 3,
                            'metric': 'pieces',
                        },
                        {
                            'name': 'cheese',
                            'quantity': 2,
                            'metric': 'ounces',
                        },
                        {
                            'name': 'avocado',
                            'quantity': 5,
                            'metric': 'tbsp',
                        },
                    ]
                }
            }
        ))

    def testRecipeStringIsName(self):
        self.assertEqual(str(self.recipe), self.recipe.getName())

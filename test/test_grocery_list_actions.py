import unittest
from actions.grocery_list_actions import GroceryListActions
from actions.recipe_generation import RecipeGenerationActions

class TestGroceryListActions(unittest.TestCase):
    def setUp(self):
        self.recipePool = {
            'breakfast': [
                {
                    'name': 'Breakfast Quesadilla',
                    'ingredients': {
                        '3': [{
                            'name': 'corn tortilla',
                            'quantity': 1,
                            'metric': 'pieces',
                        }],
                        '5': [{
                            'name': 'corn tortilla',
                            'quantity': 1,
                            'metric': 'pieces',
                        }],
                    },
                },
            ],
            'lunch': [
                {
                    'name': 'Breakfast Quesadilla',
                    'ingredients': {
                        '3': [{
                            'name': 'corn tortilla',
                            'quantity': 1,
                            'metric': 'pieces',
                        }],
                        '5': [{
                            'name': 'corn tortilla',
                            'quantity': 1,
                            'metric': 'pieces',
                        }],
                    },
                },

            ],
            'dinner': [
                {
                    'name': 'Breakfast Quesadilla',
                    'ingredients': {
                        '3': [{
                            'name': 'corn tortilla',
                            'quantity': 1,
                            'metric': 'pieces',
                        }],
                        '5': [{
                            'name': 'corn tortilla',
                            'quantity': 1,
                            'metric': 'pieces',
                        }],
                    },
                },
            ],
            'snacks': [
                {
                    'ingredients': [
                        {
                            'name': 'hard boiled egg',
                            'quantity': 1,
                            'metric': 'pieces',
                        },
                    ],
                },
            ],
        }

    def testAddWeeklyGroceries(self):
        groceryListActions = GroceryListActions()
        recipeGenerationActions = RecipeGenerationActions()
        weekRecipes = recipeGenerationActions.generateWeekOfRecipes(
            self.recipePool)
        blocks = [5, 5, 1, 5, 1]
        groceries = groceryListActions.addWeeklyGroceries(blocks, weekRecipes)
        self.assertEqual(len(groceries), 2)

import unittest
from entities.ingredient import Ingredient
from entities.snack import Snack

class TestSnacks(unittest.TestCase):
    def setUp(self):
        self.ingredients = [
            Ingredient(
                name='hard boiled egg',
                quantity=1.0,
                metric='pieces',
            ),
            Ingredient(
                name='orange',
                quantity=0.5,
                metric='pieces',
            ),
            Ingredient(
                name='peanuts',
                quantity=1.0,
                metric='sprinkle',
            ),
        ]

        self.snack = Snack(
            ingredients=[i.copy() for i in self.ingredients]
        )

    def testGetIngredients(self):
        self.assertEqual(self.snack.getIngredients(), self.ingredients)

    def testCopyEquality(self):
        self.assertEqual(self.snack.copy(), self.snack)

    def testToDict(self):
        self.assertEqual(self.snack.toDict(), {
            'ingredients': [
                {
                    'name': 'hard boiled egg',
                    'quantity': 1.0,
                    'metric': 'pieces',
                },
                {
                    'name': 'orange',
                    'quantity': 0.5,
                    'metric': 'pieces',
                },
                {
                    'name': 'peanuts',
                    'quantity': 1.0,
                    'metric': 'sprinkle',
                },
            ],
        })

    def testFromDict(self):
        self.assertEqual(self.snack, Snack.fromDict({
            'ingredients': [
                {
                    'name': 'hard boiled egg',
                    'quantity': 1.0,
                    'metric': 'pieces',
                },
                {
                    'name': 'orange',
                    'quantity': 0.5,
                    'metric': 'pieces',
                },
                {
                    'name': 'peanuts',
                    'quantity': 1.0,
                    'metric': 'sprinkle',
                },
            ],
        }))

    def testGetNameIsSnack(self):
        self.assertEqual(self.snack.getName(), 'Snack')

    def testStringSnack(self):
        self.assertEqual(str(self.snack), self.snack.getName())

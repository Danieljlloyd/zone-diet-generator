import unittest
from entities.person import Person
from entities.recipe_pool import RecipePool
from database.memory_database import MemoryDatabase

class TestMemoryDatabase(unittest.TestCase):
    def setUp(self):
        self.db = MemoryDatabase()

    def testGetPeople(self):
        self.assertEqual(self.db.getPeople(),
            [
                {
                  "name": "Shekar Seldon",
                  "blocks": [3, 3, 1, 3, 1]
                },
                {
                  "name": "Daniel Lloyd",
                  "blocks": [5, 5, 1, 5, 1]
                }
            ])

        people = [Person(x['name'], x['blocks']) for x in self.db.getPeople()]
        self.assertEqual(len(people), 2)

    def testGetRecipePool(self):
        recipePoolDict = self.db.getRecipePool()
        recipePool = RecipePool.fromDict(recipePoolDict)
        self.assertEqual(recipePoolDict, {
            "breakfast": [
              {
                "name": "Breakfast Quesadilla",
                "ingredients": {
                  "3": [
                    {
                      "name": "corn tortilla",
                      "quantity": 1,
                      "metric": "pieces"
                    },
                    {
                      "name": "corn tortilla",
                      "quantity": 1,
                      "metric": "pieces"
                    }
                  ],
                  "5": [
                    {
                      "name": "corn tortilla",
                      "quantity": 1,
                      "metric": "pieces"
                    }
                  ]

                }
              }
            ],
            "lunch": [
              {
                "name": "Breakfast Quesadilla",
                "ingredients": {
                  "3": [
                    {
                      "name": "corn tortilla",
                      "quantity": 1,
                      "metric": "pieces"
                    }
                  ],
                  "5": [
                    {
                      "name": "corn tortilla",
                      "quantity": 1,
                      "metric": "pieces"
                    }
                  ]
                }
              }
            ],
            "dinner": [
              {
                "name": "Breakfast Quesadilla",
                "ingredients": {
                  "3": [
                    {
                      "name": "corn tortilla",
                      "quantity": 1,
                      "metric": "pieces"
                    }
                  ],
                  "5": [
                    {
                      "name": "corn tortilla",
                      "quantity": 1,
                      "metric": "pieces"
                    }
                  ]
                }
              }
            ],
        "snacks": [
          {
            "ingredients": [
              {
                "name": "hard boiled egg",
                "quantity": 1,
                "metric": "pieces"
              }
            ]
          }
        ]
        })

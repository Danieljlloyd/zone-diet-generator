import unittest
from entities.recipe import Recipe
from entities.ingredient import Ingredient
from entities.recipe_pool import *
from entities.snack import Snack
import random

class TestRecipePool(unittest.TestCase):
    def setUp(self):
        self.seed = 73827
        self.oneBlockBreakfastIngredients = [
            Ingredient(
                name='corn tortillas',
                quantity=2.0,
                metric='pieces',
            ),
            Ingredient(
                name='black beans',
                quantity=0.5,
                metric='cups',
            ),
            Ingredient(
                name='onions',
                quantity=0.33,
                metric='pieces',
            ),
            Ingredient(
                name='green pepper',
                quantity=1,
                metric='pieces',
            ),
            Ingredient(
                name='eggs',
                quantity=3,
                metric='pieces',
            ),
            Ingredient(
                name='cheese',
                quantity=2,
                metric='ounces',
            ),
            Ingredient(
                name='avocado',
                quantity=5,
                metric='tbsp',
            ),
        ]

        self.twoBlockBreakfastIngredients = [
            Ingredient(
                name='corn tortillas',
                quantity=1.0,
                metric='pieces',
            ),
            Ingredient(
                name='black beans',
                quantity=0.25,
                metric='cups',
            ),
            Ingredient(
                name='eggs',
                quantity=1,
                metric='pieces',
            ),
            Ingredient(
                name='cheese',
                quantity=1,
                metric='ounces',
            ),
            Ingredient(
                name='avocado',
                quantity=1,
                metric='tbsp',
            ),
        ]

        self.breakfastMeals = [
            Recipe(
                name='Breakfast Quesadilla',
                ingredients={
                    1: self.oneBlockBreakfastIngredients,
                    2: self.twoBlockBreakfastIngredients,
                }
            ),
        ]

        self.lunchMeals = [
            Recipe(
                name='tuna sandwich',
                ingredients={
                    5: [
                        Ingredient(
                            name='canned tuna',
                            quantity=5.0,
                            metric='ounces',
                        ),
                        Ingredient(
                            name='light mayo',
                            quantity=5.0,
                            metric='tbsp',
                        ),
                        Ingredient(
                            name='bread',
                            quantity=1.0,
                            metric='slices',
                        ),
                        Ingredient(
                            name='apple',
                            quantity=1.5,
                            metric='pieces',
                        ),
                    ]
                }
            )
        ]

        self.dinnerMeals = [
            Recipe(
                name='Fresh Fish',
                ingredients={
                    2: [
                        Ingredient(
                            name='fresh fish',
                            quantity=3.0,
                            metric='ounces',
                        ),
                        Ingredient(
                            name='zucchini in herbs',
                            quantity=1.33,
                            metric='cups',
                        ),
                        Ingredient(
                            name='salad',
                            quantity=1.0,
                            metric='pieces',
                        ),
                        Ingredient(
                            name='salad dressing',
                            quantity=1.0,
                            metric='tbsp',
                        ),
                    ]
                }
            )
        ]

        self.snacks = [
            Snack(
                ingredients=[
                    Ingredient(
                        name='hard boiled egg',
                        quantity=1.0,
                        metric='pieces',
                    ),
                    Ingredient(
                        name='orange',
                        quantity=0.5,
                        metric='pieces',
                    ),
                    Ingredient(
                        name='peanuts',
                        quantity=1.0,
                        metric='sprinkle',
                    ),
                ]
            ),
            Snack(
                ingredients=[
                    Ingredient(
                        name='plan yoghurt',
                        quantity=0.5,
                        metric='cups',
                    ),
                    Ingredient(
                        name='pecans',
                        quantity=1.0,
                        metric='sprinkle',
                    ),
                ]
            ),
            Snack(
                ingredients=[
                    Ingredient(
                        name='cheese',
                        quantity=1.0,
                        metric='ounces',
                    ),
                    Ingredient(
                        name='apple',
                        quantity=0.5,
                        metric='pieces',
                    ),
                    Ingredient(
                        name='macadamia nuts',
                        quantity=1.0,
                        metric='pieces',
                    ),
                ]
            )
        ]

        self.recipePool = RecipePool(
            breakfastMeals=[m.copy() for m in self.breakfastMeals],
            lunchMeals=[m.copy() for m in self.lunchMeals],
            dinnerMeals=[m.copy() for m in self.dinnerMeals],
            snacks=[s.copy() for s in self.snacks],
            randomSeed=self.seed,
        )

    def testGetBreakfastRecipes(self):
        self.assertEqual(self.recipePool.getBreakfastMeals(),
            self.breakfastMeals)

    def testGetLunchRecipes(self):
        self.assertEqual(self.recipePool.getLunchMeals(),
            self.lunchMeals)

    def testGetDinnerMeals(self):
        self.assertEqual(self.recipePool.getDinnerMeals(),
            self.dinnerMeals)

    def testGetSnacks(self):
        self.assertEqual(self.recipePool.getSnacks(),
            self.snacks)

    def testGetRandomDinnerMeal(self):
        randomMeal = self.recipePool.getRandomMeal('dinner')
        random.seed(self.seed)
        randomIndex = random.randint(0, len(self.dinnerMeals)-1)
        self.assertEqual(randomMeal, self.dinnerMeals[randomIndex])

    def testGetRandomLunchMeal(self):
        randomMeal = self.recipePool.getRandomMeal('lunch')
        random.seed(self.seed)
        randomIndex = random.randint(0, len(self.lunchMeals)-1)
        self.assertEqual(randomMeal, self.lunchMeals[randomIndex])

    def testGetRandomBreakfastMeal(self):
        randomMeal = self.recipePool.getRandomMeal('breakfast')
        random.seed(self.seed)
        randomIndex = random.randint(0, len(self.lunchMeals)-1)
        self.assertEqual(randomMeal, self.breakfastMeals[randomIndex])

    def testGetRandomSnack(self):
        randomSnack = self.recipePool.getRandomSnack()
        random.seed(self.seed)
        randomIndex = random.randint(0, len(self.lunchMeals)-1)
        self.assertEqual(randomSnack, self.snacks[randomIndex])

    def testGetIngredientsFromRandomMeal(self):
        breakfastMeal = self.recipePool.getRandomMeal('breakfast')
        self.assertEqual(breakfastMeal.getIngredients(1),
            self.oneBlockBreakfastIngredients)

    def testGetTwoBlockSizeIngredients(self):
        breakfastMeal = self.recipePool.getRandomMeal('breakfast')
        self.assertNotEqual(breakfastMeal.getIngredients(1),
            breakfastMeal.getIngredients(2))
        self.assertEqual(breakfastMeal.getIngredients(1),
            self.oneBlockBreakfastIngredients)
        self.assertEqual(breakfastMeal.getIngredients(2),
            self.twoBlockBreakfastIngredients)

    def testRecipePoolToDict(self):
        self.assertEqual(self.recipePool.toDict(), {
            'breakfast': [r.toDict() for r in self.recipePool.breakfastMeals],
            'lunch': [r.toDict() for r in self.recipePool.lunchMeals],
            'dinner': [r.toDict() for r in self.recipePool.dinnerMeals],
            'snacks': [s.toDict() for s in self.recipePool.snacks],
        })

    def testRecipePoolFromDict(self):
        self.assertEqual(self.recipePool,
            RecipePool.fromDict(self.recipePool.toDict()))

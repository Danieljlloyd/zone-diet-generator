import unittest
from actions.recipe_generation import RecipeGenerationActions

class TestRecipeListCreation(unittest.TestCase):
    def testCreateWeekRecipeList(self):
        recipePool = {
            'breakfast': [
                {
                    'name': 'Breakfast Quesadilla',
                    'ingredients': {
                        '3': [{
                            'name': 'corn tortilla',
                            'quantity': 1,
                            'metric': 'pieces',
                        }],
                        '5': [{
                            'name': 'corn tortilla',
                            'quantity': 1,
                            'metric': 'pieces',
                        }],
                    },
                },
            ],
            'lunch': [
                {
                    'name': 'Breakfast Quesadilla',
                    'ingredients': {
                        '3': [{
                            'name': 'corn tortilla',
                            'quantity': 1,
                            'metric': 'pieces',
                        }],
                        '5': [{
                            'name': 'corn tortilla',
                            'quantity': 1,
                            'metric': 'pieces',
                        }],
                    },
                },

            ],
            'dinner': [
                {
                    'name': 'Breakfast Quesadilla',
                    'ingredients': {
                        '3': [{
                            'name': 'corn tortilla',
                            'quantity': 1,
                            'metric': 'pieces',
                        }],
                        '5': [{
                            'name': 'corn tortilla',
                            'quantity': 1,
                            'metric': 'pieces',
                        }],
                    },
                },
            ],
            'snacks': [
                {
                    'ingredients': [
                        {
                            'name': 'hard boiled egg',
                            'quantity': 1,
                            'metric': 'pieces',
                        },
                    ],
                },
            ],
        }

        recipeGenerationActions = RecipeGenerationActions()
        recipes = recipeGenerationActions.generateWeekOfRecipes(recipePool)
        self.assertEqual(len(recipes), 7)
        self.assertEqual(len(recipes[0]), 5)

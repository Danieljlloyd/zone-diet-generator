import random
import time
from entities.recipe import Recipe
from entities.snack import Snack

class RecipePool(object):
    def __init__(self, breakfastMeals, lunchMeals, dinnerMeals, snacks,
            randomSeed):

        self.breakfastMeals = breakfastMeals
        self.lunchMeals = lunchMeals
        self.dinnerMeals = dinnerMeals
        self.snacks = snacks
        random.seed(randomSeed)

    def getBreakfastMeals(self):
        return self.breakfastMeals

    def getLunchMeals(self):
        return self.lunchMeals

    def getDinnerMeals(self):
        return self.dinnerMeals

    def getSnacks(self):
        return self.snacks

    def getRandomMeal(self, mealType):
        randomIndex = random.randint(0, len(self.dinnerMeals)-1)

        if mealType == 'dinner':
            return self.dinnerMeals[randomIndex]
        elif mealType == 'lunch':
            return self.lunchMeals[randomIndex]
        elif mealType == 'breakfast':
            return self.breakfastMeals[randomIndex]

        return None

    def getRandomSnack(self):
        randomIndex = random.randint(0, len(self.dinnerMeals)-1)
        return self.snacks[randomIndex]

    def toDict(self):
        return {
            'breakfast': [r.toDict() for r in self.getBreakfastMeals()],
            'lunch': [r.toDict() for r in self.getLunchMeals()],
            'dinner': [r.toDict() for r in self.getDinnerMeals()],
            'snacks': [r.toDict() for r in self.getSnacks()],
        }

    @staticmethod
    def fromDict(recipePoolDict):
        return RecipePool(
            breakfastMeals=[Recipe.fromDict(r) for r in recipePoolDict['breakfast']],
            lunchMeals=[Recipe.fromDict(r) for r in recipePoolDict['lunch']],
            dinnerMeals=[Recipe.fromDict(r) for r in recipePoolDict['dinner']],
            snacks=[Snack.fromDict(s) for s in recipePoolDict['snacks']],
            randomSeed=time.time()
        )

    def __eq__(self, other):
        return self.breakfastMeals == other.breakfastMeals and \
            self.lunchMeals == other.lunchMeals and \
            self.dinnerMeals == other.dinnerMeals and \
            self.snacks == other.snacks

class GroceryList(object):
    def __init__(self):
        self.groceries = {}

    def getGroceries(self):
        return self.groceries

    def addIngredient(self, ingredient):
        if not ingredient.getName() in self.groceries.keys():
            self.groceries[ingredient.getName()] = {
                'quantity': ingredient.getQuantity(),
                'metric': ingredient.getMetric(),
            }
        else:
            self.groceries[ingredient.getName()]['quantity'] += ingredient.getQuantity()

class Person(object):
    def __init__(self, name, blocks):
        self.name = name
        self.blocks = blocks

    def getName(self):
        return self.name

    def getBlocks(self):
        return self.blocks

from entities.ingredient import Ingredient

class Recipe(object):
    def __init__(self, name, ingredients=None):
        self.name = name
        self.ingredients = ingredients

    def getIngredients(self, blocks):
        return self.ingredients[blocks]

    def getName(self):
        return self.name

    def copy(self):
        return Recipe(
            name=self.name,
            ingredients=self.ingredients,
        )

    def toDict(self):
        return {
            'name': self.name,
            'ingredients': {str(blocks): [i.toDict() for i in ingredients] 
                    for blocks, ingredients in self.ingredients.iteritems()}
        }

    @staticmethod
    def fromDict(dictRecipe):
        ingredientsMap = {}
        for block, ingredientsDict in dictRecipe['ingredients'].iteritems():
            ingredients = []
            for ingredientDict in ingredientsDict:
                ingredient = Ingredient.fromDict(ingredientDict)
                ingredients.append(ingredient)
            ingredientsMap[int(block)] = ingredients

        return Recipe(
            name=dictRecipe['name'],
            ingredients=ingredientsMap,
        )

    def __eq__(self, other):
        return self.getName() == other.getName() and \
            self.ingredients == other.ingredients

    def __str__(self):
        return self.getName()

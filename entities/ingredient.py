class Ingredient(object):
    def __init__(self, name, quantity, metric):
        self.name = name
        self.quantity = quantity
        self.metric = metric

    def getName(self):
        return self.name

    def getQuantity(self):
        return self.quantity

    def getMetric(self):
        return self.metric

    def copy(self):
        return Ingredient(
            name=self.name,
            quantity=self.quantity,
            metric=self.metric,
        )

    def toDict(self):
        return {
            'name': self.name,
            'quantity': self.quantity,
            'metric': self.metric,
        }

    @staticmethod
    def fromDict(self):
        return Ingredient(
            name=self['name'],
            quantity=self['quantity'],
            metric=self['metric'],
        )

    def __eq__(self, other):
        return self.name == other.name and \
            self.quantity == other.quantity and \
            self.metric == other.metric

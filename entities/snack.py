from entities.ingredient import Ingredient

class Snack(object):
    def __init__(self, ingredients):
        self.ingredients = ingredients

    def getIngredients(self):
        return self.ingredients

    def copy(self):
        return Snack(
            ingredients=self.ingredients,
        )

    def toDict(self):
        return {
            'ingredients': [i.toDict() for i in self.ingredients]
        }

    @staticmethod
    def fromDict(snackDict):
        return Snack(
            ingredients=[Ingredient.fromDict(i)
                for i in snackDict['ingredients']],
        )

    def __eq__(self, other):
        return self.ingredients == other.ingredients

    def getName(self):
        return 'Snack'

    def __str__(self):
        return self.getName()

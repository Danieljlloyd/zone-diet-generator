from entities.grocery_list import GroceryList
from entities.recipe import Recipe
from entities.snack import Snack

class GroceryListActions(object):
    def addWeeklyGroceries(self, blocks, weekRecipes):
        groceryList = GroceryList()

        for dayRecipes in weekRecipes:
            for i, blockCount in enumerate(blocks):
                try:
                    recipe = Recipe.fromDict(dayRecipes[i])
                    ingredients = recipe.getIngredients(blockCount)

                # TODO: This is gross. Clean up handling of snacks/recipes.
                except AttributeError:
                    recipe = Snack.fromDict(dayRecipes[i])
                    ingredients = recipe.getIngredients()

                for ingredient in ingredients:
                    groceryList.addIngredient(ingredient)

        return groceryList.getGroceries()

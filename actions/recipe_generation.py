from entities.recipe_pool import RecipePool

WEEK_DAYS = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday',
     'Saturday', 'Sunday']

MEALS = ['breakfast', 'lunch', 'snack', 'dinner', 'snack']

class RecipeGenerationActions(object):
    def generateWeekOfRecipes(self, recipePoolDict):
        recipePool = RecipePool.fromDict(recipePoolDict)

        week = []
        for day in WEEK_DAYS:
            dailyMeals = []
            for meal in MEALS:
                if meal == 'snack':
                    randomSnack = recipePool.getRandomSnack()
                    dailyMeals.append(randomSnack.toDict())
                else:
                    randomMeal = recipePool.getRandomMeal(meal)
                    dailyMeals.append(randomMeal.toDict())
            week.append(dailyMeals)

        return week

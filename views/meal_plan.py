from database.memory_database import MemoryDatabase
from presenters.meal_plan import MealPlanPresenter
from controllers.recipe_generation import RecipeGenerationController

class MealPlanView(object):
    def __init__(self, db):
        self.presenter = MealPlanPresenter(db)

    def render(self, weekRecipes):
        out = ''
        viewModel = self.presenter.getViewModel(weekRecipes)

        for person, mealPlan in viewModel.iteritems():
            out += person + '\n' + '='*len(person) + '\n' * 2
            for dayName, mealList in mealPlan.iteritems():
                out += dayName + '\n' + '-'*len(dayName) + '\n'
                for meal in mealList:
                    mealName = meal['name']
                    ingredientsDict = meal['ingredients']
                    out += mealName + ':\n'
                    for ingredient in ingredientsDict:
                        out += '- {quantity} {metric} of {name}\n'.format(
                            **ingredient)
                    out += '\n'
                    

        return out

if __name__ == '__main__':
    db = MemoryDatabase()
    controller = RecipeGenerationController(db)
    weekRecipes = controller.getWeekRecipes()
    view = MealPlanView(db)
    print view.render(weekRecipes)

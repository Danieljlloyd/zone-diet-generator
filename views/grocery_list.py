from database.memory_database import MemoryDatabase
from actions.recipe_generation import RecipeGenerationActions
from presenters.grocery_list import GroceryListPresenter

class GroceryListView(object):
    def __init__(self, db):
        self.presenter = GroceryListPresenter(db)

    def render(self, weekRecipes):
        out =  'Grocery List\n'
        out += '============\n'
        viewModel = self.presenter.getViewModel(weekRecipes)
        groceries = viewModel['total']
        for ingredientName, data in groceries.iteritems():
            out += '{0} {1} of {2}'.format(data['quantity'], data['metric'],
                ingredientName) + '\n'
        out += '\n'
        return out

if __name__ == '__main__':
    db = MemoryDatabase()
    recipePool = db.getRecipePool()
    recipeActions = RecipeGenerationActions()
    weekRecipes = recipeActions.generateWeekOfRecipes(recipePool)
    view = GroceryListView(db)
    print(view.render(weekRecipes))

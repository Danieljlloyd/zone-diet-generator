from datetime import datetime
from database.file_database import FileDatabase
from controllers.recipe_generation import RecipeGenerationController
from views.grocery_list import GroceryListView
from views.meal_plan import MealPlanView

def main():
    db = FileDatabase('zone_config.json')
    recipeGenerationController = RecipeGenerationController(db)
    weekRecipes = recipeGenerationController.getWeekRecipes()
    groceryListView = GroceryListView(db)
    mealPlanView = MealPlanView(db)

    date = datetime.utcnow().strftime("%Y%m%d")

    with open('data/{0}_grocery_list.txt'.format(date), 'w+') as fp:
        fp.write(groceryListView.render(weekRecipes))

    with open('data/{0}_meal_plan.txt'.format(date), 'w+') as fp:
        fp.write(mealPlanView.render(weekRecipes))

if __name__ == '__main__':
    main()

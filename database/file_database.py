import json

class FileDatabase(object):
    def __init__(self, filename):
        self.people = None
        self.recipePool = None
        with open(filename) as fp:
            config = json.load(fp)
            self.people = config['people']
            self.recipePool = config['recipe_pool']

    def getPeople(self):
        return self.people

    def getRecipePool(self):
        return self.recipePool

if __name__ == '__main__':
    db = FileDatabase('zone_config.json')
    print db.getPeople()
    print db.getRecipePool()

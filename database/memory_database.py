class MemoryDatabase(object):
    def getPeople(self):
        return [
            {
              "name": "Shekar Seldon",
              "blocks": [3, 3, 1, 3, 1]
            },
            {
              "name": "Daniel Lloyd",
              "blocks": [5, 5, 1, 5, 1]
            }
        ]

    def getRecipePool(self):
        return {
            "breakfast": [
              {
                "name": "Breakfast Quesadilla",
                "ingredients": {
                  "3": [
                    {
                      "name": "corn tortilla",
                      "quantity": 1,
                      "metric": "pieces"
                    },
                    {
                      "name": "corn tortilla",
                      "quantity": 1,
                      "metric": "pieces"
                    }
                  ],
                  "5": [
                    {
                      "name": "corn tortilla",
                      "quantity": 1,
                      "metric": "pieces"
                    }
                  ]

                }
              }
            ],
            "lunch": [
              {
                "name": "Breakfast Quesadilla",
                "ingredients": {
                  "3": [
                    {
                      "name": "corn tortilla",
                      "quantity": 1,
                      "metric": "pieces"
                    }
                  ],
                  "5": [
                    {
                      "name": "corn tortilla",
                      "quantity": 1,
                      "metric": "pieces"
                    }
                  ]
                }
              }
            ],
            "dinner": [
              {
                "name": "Breakfast Quesadilla",
                "ingredients": {
                  "3": [
                    {
                      "name": "corn tortilla",
                      "quantity": 1,
                      "metric": "pieces"
                    }
                  ],
                  "5": [
                    {
                      "name": "corn tortilla",
                      "quantity": 1,
                      "metric": "pieces"
                    }
                  ]
                }
              }
            ],
        "snacks": [
          {
            "ingredients": [
              {
                "name": "hard boiled egg",
                "quantity": 1,
                "metric": "pieces"
              }
            ]
          }
        ]
        }

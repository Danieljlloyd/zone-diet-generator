Zone Diet Generator
===================

This is a Python application that I wrote over a couple of days to generate
a random zone diet and a corresponding grocery list for that week. Designed
to synchronise two peoples meals with different block counts so that I could
use it easily with my girlfriend.

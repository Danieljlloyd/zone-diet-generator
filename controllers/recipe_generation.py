from actions.recipe_generation import RecipeGenerationActions

class RecipeGenerationController(object):
    def __init__(self, db):
        self.db = db
        self.recipeActions = RecipeGenerationActions()

    def getWeekRecipes(self):
        recipePool = self.db.getRecipePool()
        weekRecipes = self.recipeActions.generateWeekOfRecipes(recipePool)
        return weekRecipes
